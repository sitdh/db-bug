import os, logging
import pymysql

def get_connection():
    conn = None
    
    status = ''
    try:
        conn = pymysql.connect(
            host=os.getenv('DB_ENDPOINT'), 
            user=os.getenv('DB_USER'), 
            passwd=os.getenv('DB_PASSWORD'), 
            db=os.getenv('DB_NAME'), 
            connect_timeout=5
        )
        
        status = 'success'
    except Exception as e:
        status = f'failure {e}'
        
        
    logging.error(f'Connection creation {status}')
    
    return conn

def data_format(data):
    entity_list = default_entities()
        
    dataset = None
    try:
        dataset = [data[i] for i in entity_list]
    except:
        pass
        
    return dataset
    
def insert_statement():
    entity_list = "`" + "`, `".join(default_entities()) + "`"
    template = ', '.join(['%s'] * len(default_entities()))
    
    return f"INSERT INTO `betlogs_reporting` ({entity_list}) VALUES ({template})"

def default_entities():
    return ["user_id",
        "username",
        "ref",
        "game",
        "game_id",
        "game_type",
        "turnover",
        "valid_amount",
        "winloss",
        "commission",
        "total",
        "agent",
        "agent_commission",
        "master",
        "master_commission",
        "senior",
        "senior_commission",
        "subcompany",
        "subcompany_commission",
        "company",
        "company_commission",
        "agent_total",
        "master_total",
        "senior_total",
        "subcompany_total",
        "company_total",
        "up_id",
        "agent_id",
        "master_id",
        "senior_id",
        "subcompany_id",
        "company_id",
        "bettime",
        "bettime_unix",
        "caltime",
        "md5",
        "created_at"]

def new_file_collection(file_name, cursor=None):
    logging.info(f'Saving: {file_name}.txt')
    statement = 'INSERT INTO `file_input_log` (`file_id`, `process_status`) ' \
        + f'VALUES ({file_name}, "ON PROCESS")'

    cursor.execute(
        statement
    )


def update_file_collection(file_name, row_count, cursor=None):
    logging.info(f'Saving: {file_name}.txt')
    statement = 'UPDATE `file_input_log` ' \
        + f'SET `row_count` = {row_count}, ' \
        + 'process_status = "DONE" ' \
        + f'WHERE file_id = {file_name}'

    logging.info(f'Update {file_name} with {row_count}')

    cursor.execute(
        statement
    )