import boto3, os, time, sys
import json, logging, random
import formatter as fmt

from dotenv import load_dotenv

load_dotenv()

index = '0' if len(sys.argv) == 1 else sys.argv[1]

logging.basicConfig(filename=os.getenv('LOG_FILE'))

S3_BUCKET_NAME = os.getenv('BUCKET_NAME', 'testt')
sqs = boto3.resource('sqs')
queue = sqs.get_queue_by_name(QueueName=os.getenv('QUEUE_NAME', 'test'))

def queue_receive(queue, cursor=None, connect=None):
    sqs_client = boto3.client('sqs')
    s3_client = boto3.client('s3')

    all_objects = []

    MAXIMUM_DELAY = 10
    INITIAL_DELAY = random.random()
    
    delay = INITIAL_DELAY

    while True:
        records = None
        receipt_handle = None

        try:
            logging.info('Get message')
            message_response = sqs_client.receive_message(
                QueueUrl=queue.url
            ).get('Messages')

            if None == message_response or 0 == len(message_response):
                raise

            receipt_handle = message_response[0].get('ReceiptHandle')
            records = json.loads(
                message_response[0].get('Body')
            ).get('Records')

        except:
            logging.warning('No message exists')
            delay += random.random()
            receipt_handle = None
            records = None

        try:
            if records == None or receipt_handle == None:
                raise

            delay = INITIAL_DELAY

            for record in records:
                object_key = record['s3']['object'].get('key')

                logging.info(f'Receiving: {S3_BUCKET_NAME}/{object_key}')
                file_context = s3_client.get_object(
                    Bucket=S3_BUCKET_NAME,
                    Key=object_key
                )['Body']

                all_objects += [fmt.data_format(d) for d in json.load(file_context)]

            statement = fmt.insert_statement()

            cursor.executemany(
                statement,
                all_objects
            )

            logging.info('All {} records commit'.format(len(all_objects)))
            # print('All {} records commit'.format(len(all_objects)))

            logging.info('Dequeue message')
            sqs_client.delete_message(
                QueueUrl=queue.url,
                ReceiptHandle=receipt_handle
            )
            connect.commit()

        except:
            connect.rollback()
            if delay > INITIAL_DELAY:
                logging.info('Queue still empty')
            else:
                logging.error('Creation failure')

        time.sleep(min(MAXIMUM_DELAY, delay))

if __name__ == '__main__':

    conn = fmt.get_connection()
    with conn.cursor() as cur:
        logging.info('Connection exists')
        queue_receive(queue, cursor=cur, connect=conn)