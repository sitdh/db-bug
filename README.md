# โค้ดสำหรับการนำข้อมูลแบบ Raw file เข้า และ report  

ประกบด้วย 3 โฟลเดอร์ ได้แก่  

- SQL สำหรับเตรียมฐานข้อมูล เป็น DDL script (DDL: Data Definition Language) [sql]  
- นำไฟล์เข้าสู่ฐานข้อมูล [python-queue.raw-file-import-to-db]  
- สร้างฐานข้อมูลเพื่อสรุปรายงานด้วย Apache Airflow [airflow-dag.repot-builder]