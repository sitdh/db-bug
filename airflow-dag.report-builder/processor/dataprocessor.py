from email.mime import base
from genericpath import exists
import pandas as pd
import logging

from sqlalchemy.orm import Session
from processor.config import agg_columns, base_fields, sum_dict, str_dict

class DataProcessor(object):
    def data_viewer(self, data) -> pd.DataFrame:
        pass

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        pass

    def update_records(self, data, conn) -> None:
        pass

    def insert_query_builder(self, data) -> str:
        pass

class CompanySubcompanyDataProcessor(DataProcessor):

    def __init__(self):
        self.__table_name = 'mview_company2subcompany'

    def data_viewer(self, data) -> pd.DataFrame:
        return data.groupby(
            ['bettime_unix', 'company_id', 'subcompany_id', 'game_id', 'up_id']
        ).agg(agg_columns)[['username', 'user_id'] + base_fields].rename(
            columns={'user_id': 'players'}
        ).reset_index().rename(columns={'bettime_unix': 'date_unix'}).set_index([
            'date_unix', 'company_id', 'subcompany_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [], 
            'company_id': [],
            'subcompany_id': [], 
            'game_id': [], 
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['company_id'].append(str(i[1]))
            query_index['subcompany_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        company_id_list = ', '.join(set(query_index['company_id']))
        subcompany_id_list = ', '.join(set(query_index['subcompany_id']))
        up_id_list = ', '.join(set(query_index['up_id']))
        game_id_list = ', '.join(set(query_index['game_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        company_id in ({company_id_list}) and
        subcompany_id in ({subcompany_id_list}) and
        game_id in ({game_id_list}) and
        up_id in ({up_id_list})'''

        df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'company_id', 
            'subcompany_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = df.filter(items=df.index, axis=0)
        return exists_row_df + df.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'players = {players}, ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total}, ' \
            + 'stake_count = {stake_count} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (company_id = {company_id}) ' \
            + 'AND (subcompany_id = {subcompany_id}) ' \
            + 'AND (game_id = {game_id}) ' \
            + 'AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part =  f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `subcompany_id`, `players`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `stake_count`, `game_id`, `up_id`, `company_id`) ' \
            + 'VALUES '
            
        values_part = '({date_unix}, {subcompany_id}, {players}, ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {stake_count}, {game_id}, {up_id}, {company_id})'

        return insert_part + ', '.join(
            [values_part.format(**k) for k in data.reset_index().to_dict('records')]
        )

class SeniorMasterDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_senior2master'

    def data_viewer(self, data) -> pd.DataFrame:
        return data.groupby(
            ['bettime_unix', 'senior_id', 'master_id', 'game_id', 'up_id']
        ).agg(agg_columns)[['username', 'user_id'] + base_fields].rename(
            columns={'user_id': 'players'}
        ).reset_index().rename(columns={'bettime_unix': 'date_unix'}).set_index([
            'date_unix', 'senior_id', 'master_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {'date_unix': [], 'master_id': [], 'game_id': [], 'up_id': [], 'senior_id': []}

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['senior_id'].append(str(i[1]))
            query_index['master_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        master_id_list = ', '.join(set(query_index['master_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        up_id_list = ', '.join(set(query_index['up_id']))
        senior_id_list = ', '.join(set(query_index['senior_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        master_id in ({master_id_list}) and
        game_id in ({game_id_list}) and
        up_id in ({up_id_list}) and
        senior_id in ({senior_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'senior_id', 
            'master_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'players = {players}, ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total}, ' \
            + 'stake_count = {stake_count} ' \
            + 'WHERE (date_unix = {date_unix}) AND ' \
            + '(senior_id = {senior_id}) AND ' \
            + '(up_id = {up_id}) AND ' \
            + '(master_id = {master_id}) AND ' \
            + '(game_id = {game_id});'

        with Session(bind=conn) as session:
            session.begin()
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_path = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `players`, `master_id`, `game_id`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `stake_count`, `senior_id`, `up_id`) ' \
            + 'VALUES ' 
            
        values_template = '({date_unix}, {players}, {master_id}, {game_id}, ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {stake_count}, {senior_id}, {up_id})'

        return insert_path + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class SeniorMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_senior2member'

    def data_viewer(self, data) -> pd.DataFrame:
        count_uniq = {'user_id': 'first', 'stake_count': 'count'}

        data = data.groupby(
            ['bettime_unix', 'senior_id', 'user_id', 'game_id', 'up_id']
        ).agg({**sum_dict, **str_dict, **count_uniq})[
            ['username'] + base_fields
        ].reset_index().rename(
            columns={'bettime_unix': 'date_unix'}
        ).set_index([
            'date_unix', 'senior_id', 'user_id', 'game_id', 'up_id'
        ])

        return data

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'senior_id': [],
            'user_id': [],
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['senior_id'].append(str(i[1]))
            query_index['user_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        senior_id_list = ', '.join(set(query_index['senior_id']))
        user_id_list = ', '.join(set(query_index['user_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        senior_id in ({senior_id_list}) and
        user_id in ({user_id_list}) and
        game_id in ({game_id_list}) and
        up_id in ({up_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'senior_id', 
            'user_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET  ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) AND (senior_id = {senior_id}) ' \
            + 'AND (user_id = {user_id}) AND (game_id = {game_id}) AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `senior_id`, `user_id`, `game_id`, `username`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `up_id`) ' \
            + 'VALUES '
        
        values_template = '({date_unix}, {senior_id}, {user_id}, {game_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {up_id})'

        return insert_part + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class MasterMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_master2member'

    def data_viewer(self, data) -> pd.DataFrame:
        count_uniq = {'user_id': 'first', 'stake_count': 'count'}

        # return data[data['up_id'] != 5].groupby(
        return data.groupby(
            ['bettime_unix', 'master_id', 'user_id', 'game_id', 'up_id']
        ).agg({**sum_dict, **str_dict, **count_uniq})[
            ['username'] + base_fields
        ].reset_index().rename(
            columns={'bettime_unix': 'date_unix'}
        ).set_index([
            'date_unix', 'master_id', 'user_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'master_id': [],
            'user_id': [],
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['master_id'].append(str(i[1]))
            query_index['user_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        master_id_list = ', '.join(set(query_index['master_id']))
        user_id_list = ', '.join(set(query_index['user_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''SELECT * FROM {self.__table_name}
        WHERE date_unix IN ({date_unix_list}) AND
        master_id IN ({master_id_list}) AND
        user_id IN ({user_id_list}) AND
        game_id IN ({game_id_list}) AND 
        up_id IN ({up_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'master_id', 
            'user_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET  ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) AND (master_id = {master_id}) ' \
            + 'AND (user_id = {user_id}) AND (game_id = {game_id}) AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `master_id`, `user_id`, `game_id`, `username`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `up_id`) ' \
            + 'VALUES '
        
        values_template = '({date_unix}, {master_id}, {user_id}, {game_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {up_id})'

        return insert_part + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class MasterAgentDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_master2agent'

    def data_viewer(self, data) -> pd.DataFrame:
        return data[data['up_id'] != 5].groupby(
            ['bettime_unix', 'master_id', 'agent_id', 'game_id', 'up_id']
        ).agg(agg_columns)[['username', 'user_id'] + base_fields].rename(
            columns={'user_id': 'players'}
        ).reset_index().rename(columns={'bettime_unix': 'date_unix'}).set_index([
            'date_unix', 'master_id', 'agent_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [], 
            'master_id': [], 
            'agent_id': [], 
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['master_id'].append(str(i[1]))
            query_index['agent_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        agent_id_list = ', '.join(set(query_index['agent_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        master_id_list = ', '.join(set(query_index['master_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        agent_id in ({agent_id_list}) and
        master_id in ({master_id_list}) and
        up_id in ({up_id_list}) and
        game_id in ({game_id_list})'''
        
        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'master_id', 
            'agent_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'players = {players}, ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total}, ' \
            + 'stake_count = {stake_count} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (agent_id = {agent_id}) ' \
            + 'AND (game_id = {game_id}) ' \
            + 'AND (master_id = {master_id}) ' \
            + 'AND (up_id = {up_id})' \

        with Session(bind=conn) as session:
            session.begin()
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_path = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `players`, `agent_id`, `game_id`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `stake_count`, `master_id`, `up_id`) ' \
            + 'VALUES ' 
            
        values_template = '({date_unix}, {players}, {agent_id}, {game_id}, ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {stake_count}, {master_id}, {up_id})'

        return insert_path + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class AgentMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_agent2member'

    def data_viewer(self, data) -> pd.DataFrame:
        count_uniq = {'user_id': 'first', 'stake_count': 'count'}

        # return data[data['up_id'] != 5].groupby(
        return data.groupby(
            ['bettime_unix', 'agent_id', 'user_id', 'game_id', 'up_id']
        ).agg({**sum_dict, **str_dict, **count_uniq})[
            ['username'] + base_fields
        ].reset_index().rename(
            columns={'bettime_unix': 'date_unix'}
        ).set_index([
            'date_unix', 'agent_id', 'user_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'agent_id': [],
            'user_id': [],
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['agent_id'].append(str(i[1]))
            query_index['user_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        agent_id_list = ', '.join(set(query_index['agent_id']))
        user_id_list = ', '.join(set(query_index['user_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''SELECT * FROM {self.__table_name}
        WHERE date_unix IN ({date_unix_list}) AND
        agent_id IN ({agent_id_list}) AND
        user_id IN ({user_id_list}) AND
        up_id IN ({up_id_list}) AND
        game_id IN ({game_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index(['date_unix', 'agent_id', 'user_id', 'game_id', 'up_id']).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET  ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) AND (agent_id = {agent_id}) ' \
            + 'AND (user_id = {user_id}) AND (game_id = {game_id}) AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `agent_id`, `user_id`, `game_id`, `username`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `up_id`) ' \
            + 'VALUES '
        
        values_template = '({date_unix}, {agent_id}, {user_id}, {game_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {up_id})'

        return insert_part + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class CompanyMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_company2member'

    def data_viewer(self, data) -> pd.DataFrame:
        count_uniq = {'user_id': 'first', 'stake_count': 'count'}

        # return data[data['up_id'] != 5].groupby(
        return data.groupby(
            ['bettime_unix', 'company_id', 'user_id', 'game_id', 'up_id']
        ).agg({**sum_dict, **str_dict, **count_uniq})[
            ['username'] + base_fields
        ].reset_index().rename(
            columns={'bettime_unix': 'date_unix'}
        ).set_index([
            'date_unix', 'company_id', 'user_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'company_id': [],
            'user_id': [],
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['company_id'].append(str(i[1]))
            query_index['user_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        company_id_list = ', '.join(set(query_index['company_id']))
        user_id_list = ', '.join(set(query_index['user_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''SELECT * FROM mview_company2member
        WHERE date_unix IN ({date_unix_list}) AND
        company_id IN ({company_id_list}) AND
        user_id IN ({user_id_list}) AND
        up_id IN ({up_id_list}) AND
        game_id IN ({game_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index(['date_unix', 'company_id', 'user_id', 'game_id', 'up_id']).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET  ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) AND (company_id = {company_id}) ' \
            + 'AND (user_id = {user_id}) AND (game_id = {game_id}) AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `company_id`, `user_id`, `game_id`, `username`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `up_id`) ' \
            + 'VALUES '
        
        values_template = '({date_unix}, {company_id}, {user_id}, {game_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {up_id})'

        return insert_part + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class SubcompanySeniorDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_subcompany2senior'

    def data_viewer(self, data) -> pd.DataFrame:
        # return data[data['up_id'] != 5].groupby(
        return data.groupby(
            ['bettime_unix', 'subcompany_id', 'senior_id', 'game_id', 'up_id']
        ).agg(agg_columns)[['username', 'user_id'] + base_fields].rename(
            columns={'user_id': 'players'}
        ).reset_index().rename(columns={'bettime_unix': 'date_unix'}).set_index([
            'date_unix', 'subcompany_id', 'senior_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [], 
            'subcompany_id': [], 
            'senior_id': [], 
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['subcompany_id'].append(str(i[1]))
            query_index['senior_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        senior_id_list = ', '.join(set(query_index['senior_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        subcompany_id_list = ', '.join(set(query_index['subcompany_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        senior_id in ({senior_id_list}) and
        subcompany_id in ({subcompany_id_list}) and
        up_id in ({up_id_list}) and
        game_id in ({game_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'subcompany_id', 
            'senior_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'players = {players}, ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total}, ' \
            + 'stake_count = {stake_count} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (senior_id = {senior_id}) ' \
            + 'AND (game_id = {game_id}) ' \
            + 'AND (subcompany_id = {subcompany_id}) ' \
            + 'AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            session.begin()
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_path = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `players`, `senior_id`, `game_id`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `stake_count`, `subcompany_id`, `up_id`) ' \
            + 'VALUES ' 
            
        values_template = '({date_unix}, {players}, {senior_id}, {game_id}, ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {stake_count}, {subcompany_id}, {up_id})'

        return insert_path + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class SubcompanyMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_subcompany2member'

    def data_viewer(self, data) -> pd.DataFrame:
        # return data[data['up_id'] != 5].groupby(
        return data.groupby(
            ['bettime_unix', 'subcompany_id', 'user_id', 'game_id', 'up_id']
        ).agg(agg_columns)[
            ['username'] + base_fields
        ].reset_index().rename(
            columns={'bettime_unix': 'date_unix'}
        ).set_index([
            'date_unix', 'subcompany_id', 'user_id', 'game_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [], 
            'subcompany_id': [], 
            'user_id': [], 
            'game_id': [],
            'up_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['subcompany_id'].append(str(i[1]))
            query_index['user_id'].append(str(i[2]))
            query_index['game_id'].append(str(i[3]))
            query_index['up_id'].append(str(i[4]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        user_id_list = ', '.join(set(query_index['user_id']))
        game_id_list = ', '.join(set(query_index['game_id']))
        subcompany_id_list = ', '.join(set(query_index['subcompany_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        user_id in ({user_id_list}) and
        subcompany_id in ({subcompany_id_list}) and
        up_id in ({up_id_list}) and
        game_id in ({game_id_list})'''

        db_df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'subcompany_id', 
            'user_id', 
            'game_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if db_df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = db_df.filter(items=data.index, axis=0)
        return exists_row_df + data.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (user_id = {user_id}) ' \
            + 'AND (game_id = {game_id}) ' \
            + 'AND (subcompany_id = {subcompany_id}) ' \
            + 'AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            session.begin()
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_path = f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `user_id`, `game_id`, `username`, `turnover`, ' \
            + '`valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`, `subcompany_id`, `up_id`) ' \
            + 'VALUES ' 
            
        values_template = '({date_unix}, {user_id}, {game_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total}, {subcompany_id}, {up_id})'

        return insert_path + ', '.join(
            [values_template.format(**k) for k in data.reset_index().to_dict('records')]
        )

class GodMemberDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_god2member'

    def data_viewer(self, data) -> pd.DataFrame:
        custom = base_fields.copy()
        custom.remove('stake_count')

        return data.groupby([
            'bettime_unix', 'user_id', 'up_id',
        ]).agg({
            **sum_dict,
            **str_dict,
        })[['username'] + custom].reset_index().rename(columns={
            'bettime_unix': 'date_unix'
        }).set_index([
            'date_unix', 'user_id', 'up_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'user_id': [],
            'up_id': []
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['user_id'].append(str(i[1]))
            query_index['up_id'].append(str(i[2]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        member_id_list = ', '.join(set(query_index['user_id']))
        up_id_list = ', '.join(set(query_index['up_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        user_id in ({member_id_list}) and
        up_id in ({up_id_list})'''

        df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'user_id', 
            'up_id'
        ]).drop(['created_at', 'updated_at'], axis=1)

        if df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = df.filter(items=df.index, axis=0)
        return exists_row_df + df.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (user_id = {user_id}) ' \
            + 'AND (up_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part =  f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `user_id`, `up_id`, `username`, ' \
            + '`turnover`, `valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`) ' \
            + 'VALUES '
            
        values_part = '({date_unix}, {user_id}, {up_id}, "{username}", ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total})'

        return insert_part + ', '.join(
            [values_part.format(**k) for k in data.reset_index().to_dict('records')]
        )

class GodGameDataProcessor(DataProcessor):
    def __init__(self):
        self.__table_name = 'mview_god2game'

    def data_viewer(self, data) -> pd.DataFrame:
        custom = base_fields.copy()
        custom.remove('stake_count')

        return data.groupby([
            'bettime_unix', 'game_id',
        ]).agg({
            **sum_dict,
            **str_dict,
        })[custom].reset_index().rename(columns={
            'bettime_unix': 'date_unix'
        }).set_index([
            'date_unix', 'game_id'
        ])

    def update_record_remain(self, data, conn) -> pd.DataFrame:
        index_keys = data.index.to_list()
        query_index = {
            'date_unix': [],
            'game_id': [],
        }

        for i in index_keys:
            query_index['date_unix'].append(str(i[0]))
            query_index['game_id'].append(str(i[1]))

        date_unix_list = ','.join(set(query_index['date_unix']))
        game_id_list = ', '.join(set(query_index['game_id']))

        duplicate_query = f'''select * from {self.__table_name}
        where date_unix in ({date_unix_list}) and
        game_id in ({game_id_list})'''

        df = pd.read_sql(
            duplicate_query,
            conn
        ).set_index([
            'date_unix', 
            'game_id', 
        ]).drop(['created_at', 'updated_at'], axis=1)

        if df.shape[0] == 0:
            return pd.DataFrame([])

        exists_row_df = df.filter(items=df.index, axis=0)
        return exists_row_df + df.filter(items=exists_row_df.index, axis=0)

    def update_records(self, data, conn) -> None:
        update_template = f'UPDATE `{self.__table_name}` SET ' \
            + 'turnover = {turnover}, ' \
            + 'valid_amount = {valid_amount}, ' \
            + 'winloss = {winloss}, ' \
            + 'commission = {commission}, ' \
            + 'total = {total}, ' \
            + 'agent = {agent}, ' \
            + 'agent_commission = {agent_commission}, ' \
            + 'agent_total = {agent_total}, ' \
            + 'master = {master}, ' \
            + 'master_commission = {master_commission}, ' \
            + 'master_total = {master_total}, ' \
            + 'senior = {senior}, ' \
            + 'senior_commission = {senior_commission}, ' \
            + 'senior_total = {senior_total}, ' \
            + 'subcompany = {subcompany}, ' \
            + 'subcompany_commission = {subcompany_commission}, ' \
            + 'subcompany_total = {subcompany_total}, ' \
            + 'company = {company}, ' \
            + 'company_commission = {company_commission}, ' \
            + 'company_total = {company_total} ' \
            + 'WHERE (date_unix = {date_unix}) ' \
            + 'AND (game_id = {up_id})'

        with Session(bind=conn) as session:
            for k in data.reset_index().to_dict('records'):
                session.execute(update_template.format(**k))
            session.commit()

    def insert_query_builder(self, data) -> str:
        insert_part =  f'INSERT INTO `{self.__table_name}` (' \
            + '`date_unix`, `game_id`, ' \
            + '`turnover`, `valid_amount`, `winloss`, `commission`, `total`, `agent`, ' \
            + '`agent_commission`, `agent_total`, `master`, `master_commission`, ' \
            + '`master_total`, `senior`, `senior_commission`, `senior_total`, ' \
            + '`subcompany`, `subcompany_commission`, `subcompany_total`, `company`, ' \
            + '`company_commission`, `company_total`) ' \
            + 'VALUES '
            
        values_part = '({date_unix}, {game_id}, ' \
            + '{turnover}, {valid_amount}, {winloss}, {commission}, {total}, {agent}, ' \
            + '{agent_commission}, {agent_total}, {master}, {master_commission}, ' \
            + '{master_total}, {senior}, {senior_commission}, {senior_total}, {subcompany}, ' \
            + '{subcompany_commission}, {subcompany_total}, {company}, {company_commission}, ' \
            + '{company_total})'

        return insert_part + ', '.join(
            [values_part.format(**k) for k in data.reset_index().to_dict('records')]
        )