from curses import keyname
from sqlite3 import connect
import sqlalchemy as sc
import pandas as pd
import os, logging

from processor.dataprocessor import DataProcessor

class DataBuilder(object):
    def __init__(self, conn=None, raw_data_table=None, batch_size=0):
        self._max_id = -1
        self._status = None
        self._raw_data_table = 'betlogs_06' if raw_data_table == None else raw_data_table
        self._conn = conn
        self._batch_size = batch_size

        metadata = sc.MetaData(bind=conn)

        # self.convert_property = sc.Table('conf_convert_property', metadata,
        self.convert_property = sc.Table('conf_convert_property', metadata,
            sc.Column('id', sc.BigInteger, primary_key=True),
            sc.Column('keyname', sc.String),
            sc.Column('value', sc.String),
        )

    @property
    def conn(self):
        return self._conn

    @conn.setter
    def conn(self, conn):
        self._conn = conn

    @property
    def max_id(self):
        if self._max_id == -1:
            s = sc.select(self.convert_property).where(
                self.convert_property.c.keyname == 'current_process_id'
            ).limit(1)

            self._max_id = int([r for r in self.conn.execute(s)].pop()[2])

        return self._max_id

    @max_id.setter
    def max_id(self, v):
        if v == -1 or v <= self._max_id:
            return

        s = self.convert_property.update().values(
            value=v
        ).where(
            self.convert_property.c.keyname == 'current_process_id'
        )

        self.conn.execute(s)

    @property
    def batch_size(self):
        return self._batch_size

    @property
    def process_status(self):
        if self._status == None:
            s = sc.select(self.convert_property).where(
                self.convert_property.c.keyname == 'process_status'
            ).limit(1)

            self._status = [r for r in self.conn.execute(s)].pop()[2].strip().lower()

        return self._status

    @process_status.setter
    def process_status(self, v):
        v = v.lower() if v.lower() in ('offline', 'online')  else 'offline'

        s = self.convert_property.update().values(
            value=v,
        ).where(self.convert_property.c.keyname == 'process_status')

        self.conn.execute(s)

        self._status = bool(v)

    def __select_query(self, max_id=-1, batch_size=0):
        max_id = max_id if max_id >= 0 else self.max_id

        max_id = self.max_id+1
        batch_size = self._batch_size if 0 == batch_size else batch_size

        statement = f'SELECT * FROM {self._raw_data_table} WHERE id BETWEEN {max_id} AND {max_id+batch_size}'
        logging.info(f'SQL: {statement}')
        print(statement)

        return statement

    def raw_data(self):
        df = pd.read_sql(self.__select_query(
            self.max_id
        ), self.conn).rename(columns={'id': 'stake_count'})

        dateshot = {}
        def convert_datetotimestamp(d):
            d_str = str(d)[:10]

            if d_str not in dateshot:
                dateshot[d_str] = pd.to_datetime(d_str).value // 10 ** 9

            return dateshot[d_str]

        df['bettime_unix'] = df.bettime.apply(convert_datetotimestamp)

        return df

    def process(self, data, processor: DataProcessor, conn):

        new_view = processor.data_viewer(data)
        update_list = processor.update_record_remain(new_view, conn)

        if update_list.shape[0] == 0:
            insert_data = new_view.copy()
        else:
            insert_data = new_view[~new_view.index.isin(update_list.index)]
            processor.update_records(
                update_list, conn
            )

        logging.info('-- SHAPE insert -- ' + str(insert_data.shape[0]))
        if insert_data.shape[0] > 0:
            conn.execute(
                processor.insert_query_builder(insert_data)
            )