import pandas as pd

sum_list = [
  'turnover', 'valid_amount', 'winloss', 'commission', 'total', 'agent',  'agent_commission', 
  'agent_total', 'agent_commission', 'master_commission', 'master', 'master_total', 'senior', 'senior_commission', 
  'senior_total', 'subcompany', 'subcompany_commission', 'subcompany_total', 'company', 
  'company_commission', 'company_total',
]
sum_dict = {k: 'sum' for k in sum_list}

str_dict = {
  'username': 'first', 
  'company_id': 'first', 
  'subcompany_id': 'first', 
  'master_id': 'first', 
  'bettime_unix': 'first', 
  'senior_id': 'first',
  'up_id': 'first',
}
count_unique_dict = {'user_id': pd.Series.nunique, 'stake_count': 'count'}

filtered_keys = list(sum_dict.keys()) \
  + list(str_dict.keys()) \
  + list(count_unique_dict.keys())

agg_columns = {**sum_dict, **str_dict, **count_unique_dict}

base_fields = [
  'stake_count', 'turnover', 'valid_amount', 'winloss', 
  'commission', 'total', 'agent', 
  'agent_commission', 'agent_total', 'master', 
  'master_commission', 'master_total', 'senior', 
  'senior_commission', 'senior_total', 'subcompany',
  'subcompany_commission', 'subcompany_total', 'company',
  'company_commission', 'company_total'
]
