from genericpath import isfile
from airflow.decorators import dag, task
from airflow.hooks.mysql_hook import MySqlHook
from datetime import datetime

from airflow.hooks.base import BaseHook
from airflow.models.variable import Variable


import logging, tempfile, os
import pandas as pd
import numpy as np
import sqlalchemy as sa

from dotenv import load_dotenv
load_dotenv()

from processor.databuilder import DataBuilder
from processor.dataprocessor import AgentMemberDataProcessor, \
    CompanyMemberDataProcessor, \
    CompanySubcompanyDataProcessor, \
    GodGameDataProcessor, \
    GodMemberDataProcessor, \
    MasterAgentDataProcessor, \
    MasterMemberDataProcessor, \
    SeniorMasterDataProcessor, \
    SeniorMemberDataProcessor, \
    SubcompanyMemberDataProcessor, \
    SubcompanySeniorDataProcessor

def _report_process_b(a):
    return sum(a)/len(a)

def _report_process_c(a):
    return _report_process_a(a) / _report_process_b(a)

def _report_process_d(a):
    return _report_process_c(a) * 1.2

def load_data(file_location):
    return pd.read_csv(file_location)

@dag(
    schedule_interval='*/5 * * * *',
    start_date=datetime(2022, 7, 1),
    catchup=True,
    tags=['report']
)
def report_builder_pipeline():
    report_conn = BaseHook.get_connection('report_db')
    engine = sa.create_engine(
        'mysql+pymysql://{user}:{password}@{host}/{dbname}'.format(
            user=report_conn.login,
            password=report_conn.password,
            host=report_conn.host,
            dbname=report_conn.schema
        ),
        pool_size=20
    )

    # session_factory = sa.orm.sessionmaker(bind=engine)
    # Session = sa.orm.scoped_session(session_factory)
    builder = DataBuilder(
        raw_data_table=Variable.get('RAW_DATA_TABLE'),
        batch_size=int(Variable.get('BATCH_SIZE'))
    )

    @task()
    def query_bulk_data():

        with engine.connect() as conn:
            builder.conn = conn

            logging.info('Querry data')
            df = pd.DataFrame([]) if 'online' == builder.process_status \
                else builder.raw_data()

            file_location = None
            max_id = 0

            logging.info('--- Shape --- ' + str(df.shape[0]))
            
            if df.shape[0] > 0:
                temp = tempfile.NamedTemporaryFile(
                    prefix='reportbuilder-', 
                    delete=False
                )
                
                file_location = temp.name
                max_id = int(df.stake_count.max())

                builder.process_status = 'online'
                builder.max_id = max_id

                logging.info(f'File location: {file_location}')
                logging.info(f'Max Id: {max_id}')

                df.to_csv(file_location)

            else:
                file_location = '-EMPTY-'

            shape = df.shape[0]

            Variable.update('CURRENT_DATA_PATH', str(file_location))
            Variable.update('DATA_LENGTH', int(df.shape[0]))
            Variable.update('MAXIMUM_ID', max_id)

        return {
            'data_path': str(file_location), 
            'len': int(df.shape[0]), 
            'max_id': int(max_id),
        }

    @task()
    def comp2subcomp_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                CompanySubcompanyDataProcessor(),
                conn
            )

        return True 

    @task()
    def senior2master_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                SeniorMasterDataProcessor(),
                conn
            )

        return True 

    @task()
    def senior2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                SeniorMemberDataProcessor(),
                conn
            )

        return True 

    @task()
    def master2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                MasterMemberDataProcessor(),
                conn
            )

        return True 

    @task()
    def agent2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                AgentMemberDataProcessor(),
                conn
            )

        return True 

    @task()
    def company2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                CompanyMemberDataProcessor(),
                conn
            )

        return True 

    @task()
    def subcompany2senior_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                SubcompanySeniorDataProcessor(),
                conn
            )

        return True 

    @task()
    def master2agent_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                MasterAgentDataProcessor(),
                conn
            )

        return True 

    @task
    def subcompany2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                SubcompanyMemberDataProcessor(),
                conn
            )

        return True 

    @task
    def god2member_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                GodMemberDataProcessor(),
                conn
            )

        return True 

    @task
    def god2game_mateiralized_view_builder(dataset):
        if dataset['len'] <= 0:
            return False

        df = load_data(dataset['data_path'])
        logging.info('-- god2game -- ' + str(df.shape[0]))
        with engine.connect() as conn:
            builder.conn = conn
            builder.process(
                df,
                GodGameDataProcessor(),
                conn
            )

        return True 

    @task()
    def sum_report_process(dataset):
        max_id = int(Variable.get('MAXIMUM_ID'))
        logging.info(f"dataset: max_id")
        with engine.connect() as conn:
            builder.conn = conn

            builder.max_id = max_id
            builder.process_status = 'offline'

        if os.path.isfile(Variable.get('CURRENT_DATA_PATH')):
            os.remove(Variable.get('CURRENT_DATA_PATH'))

        Variable.update('CURRENT_DATA_PATH', '-EMPTY-')
        Variable.update('DATA_LENGTH', 0)


    processed_data = query_bulk_data()
    sum_report_process([
        comp2subcomp_mateiralized_view_builder(processed_data),
        senior2master_mateiralized_view_builder(processed_data),
        subcompany2senior_mateiralized_view_builder(processed_data),
        master2agent_mateiralized_view_builder(processed_data),
        god2game_mateiralized_view_builder(processed_data),
        god2member_mateiralized_view_builder(processed_data),
        agent2member_mateiralized_view_builder(processed_data),
        company2member_mateiralized_view_builder(processed_data),
        senior2member_mateiralized_view_builder(processed_data),
        master2member_mateiralized_view_builder(processed_data),
        subcompany2member_mateiralized_view_builder(processed_data),
    ])

report_dag = areport_builder_pipeline()
logging.info(report_dag)