CREATE DATABASE  IF NOT EXISTS `betlogs_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `betlogs_db`;
-- MySQL dump 10.13  Distrib 8.0.30, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: airflow
-- ------------------------------------------------------
-- Server version	8.0.29

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `betlogs_reporting`
--

DROP TABLE IF EXISTS `betlogs_reporting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `betlogs_reporting` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `user_id` int NOT NULL DEFAULT '0',
  `ref` varchar(50) NOT NULL,
  `game` varchar(30) NOT NULL,
  `game_id` int NOT NULL DEFAULT '0',
  `game_type` varchar(30) NOT NULL,
  `agent_id` int NOT NULL DEFAULT '0',
  `master_id` int NOT NULL DEFAULT '0',
  `senior_id` int NOT NULL DEFAULT '0',
  `subcompany_id` int NOT NULL DEFAULT '0',
  `company_id` int NOT NULL DEFAULT '0',
  `turnover` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `valid_amount` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `winloss` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `agent` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `agent_commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `agent_total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `master` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `master_commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `master_total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `senior` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `senior_commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `senior_total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `subcompany` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `subcompany_commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `subcompany_total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `company` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `company_commission` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `company_total` decimal(20,4) NOT NULL DEFAULT '0.0000',
  `up_id` int NOT NULL DEFAULT '0',
  `bettime` datetime NOT NULL COMMENT '+7 เวลาไทย',
  `bettime_unix` bigint NOT NULL DEFAULT '0',
  `caltime` datetime NOT NULL COMMENT '+7 เวลาไทย',
  `md5` char(32) NOT NULL,
  `created_at` datetime NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_betlogs_reporting_game_id` (`game_id`),
  KEY `idx_betlogs_reporting_user_id` (`user_id`),
  KEY `idx_betlogs_reporting_ref` (`ref`),
  KEY `idx_betlogs_reporting_winloss` (`winloss`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `betlogs_reporting`
--

LOCK TABLES `betlogs_reporting` WRITE;
/*!40000 ALTER TABLE `betlogs_reporting` DISABLE KEYS */;
/*!40000 ALTER TABLE `betlogs_reporting` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conf_convert_property`
--

DROP TABLE IF EXISTS `conf_convert_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conf_convert_property` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `keyname` varchar(20) NOT NULL,
  `value` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conf_convert_property`
--

LOCK TABLES `conf_convert_property` WRITE;
/*!40000 ALTER TABLE `conf_convert_property` DISABLE KEYS */;
INSERT INTO `conf_convert_property` VALUES (1,'process_status','offline','2022-08-14 14:43:05','2022-08-17 06:03:35'),(2,'current_process_id','0','2022-08-14 14:43:05','2022-08-17 06:03:35');
/*!40000 ALTER TABLE `conf_convert_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `file_input_log`
--

DROP TABLE IF EXISTS `file_input_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `file_input_log` (
  `file_id` bigint NOT NULL,
  `process_status` varchar(20) DEFAULT 'FIALED',
  `row_count` int DEFAULT '0',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `file_input_log`
--

LOCK TABLES `file_input_log` WRITE;
/*!40000 ALTER TABLE `file_input_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `file_input_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_agent2member`
--

DROP TABLE IF EXISTS `mview_agent2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_agent2member` (
  `date_unix` bigint NOT NULL,
  `agent_id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`agent_id`,`user_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_agent2member`
--

LOCK TABLES `mview_agent2member` WRITE;
/*!40000 ALTER TABLE `mview_agent2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_agent2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_company2member`
--

DROP TABLE IF EXISTS `mview_company2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_company2member` (
  `date_unix` bigint NOT NULL,
  `company_id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`company_id`,`user_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_company2member`
--

LOCK TABLES `mview_company2member` WRITE;
/*!40000 ALTER TABLE `mview_company2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_company2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_company2subcompany`
--

DROP TABLE IF EXISTS `mview_company2subcompany`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_company2subcompany` (
  `date_unix` bigint NOT NULL,
  `company_id` int NOT NULL,
  `subcompany_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `players` int DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `stake_count` int DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`company_id`,`subcompany_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_company2subcompany`
--

LOCK TABLES `mview_company2subcompany` WRITE;
/*!40000 ALTER TABLE `mview_company2subcompany` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_company2subcompany` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_god2game`
--

DROP TABLE IF EXISTS `mview_god2game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_god2game` (
  `date_unix` bigint NOT NULL,
  `game_id` int NOT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`game_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_god2game`
--

LOCK TABLES `mview_god2game` WRITE;
/*!40000 ALTER TABLE `mview_god2game` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_god2game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_god2member`
--

DROP TABLE IF EXISTS `mview_god2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_god2member` (
  `date_unix` bigint NOT NULL,
  `user_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`user_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_god2member`
--

LOCK TABLES `mview_god2member` WRITE;
/*!40000 ALTER TABLE `mview_god2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_god2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_master2agent`
--

DROP TABLE IF EXISTS `mview_master2agent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_master2agent` (
  `date_unix` bigint NOT NULL,
  `master_id` int NOT NULL,
  `agent_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `stake_count` int DEFAULT NULL,
  `players` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`master_id`,`agent_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_master2agent`
--

LOCK TABLES `mview_master2agent` WRITE;
/*!40000 ALTER TABLE `mview_master2agent` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_master2agent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_master2member`
--

DROP TABLE IF EXISTS `mview_master2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_master2member` (
  `date_unix` bigint NOT NULL,
  `master_id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`master_id`,`user_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_master2member`
--

LOCK TABLES `mview_master2member` WRITE;
/*!40000 ALTER TABLE `mview_master2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_master2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_senior2master`
--

DROP TABLE IF EXISTS `mview_senior2master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_senior2master` (
  `date_unix` bigint NOT NULL,
  `senior_id` int NOT NULL,
  `master_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `players` int NOT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `stake_count` int DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`senior_id`,`master_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_senior2master`
--

LOCK TABLES `mview_senior2master` WRITE;
/*!40000 ALTER TABLE `mview_senior2master` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_senior2master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_senior2member`
--

DROP TABLE IF EXISTS `mview_senior2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_senior2member` (
  `date_unix` bigint NOT NULL,
  `senior_id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) DEFAULT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`senior_id`,`user_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_senior2member`
--

LOCK TABLES `mview_senior2member` WRITE;
/*!40000 ALTER TABLE `mview_senior2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_senior2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_subcompany2member`
--

DROP TABLE IF EXISTS `mview_subcompany2member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_subcompany2member` (
  `date_unix` bigint NOT NULL,
  `subcompany_id` int NOT NULL,
  `user_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `username` varchar(20) NOT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`subcompany_id`,`user_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_subcompany2member`
--

LOCK TABLES `mview_subcompany2member` WRITE;
/*!40000 ALTER TABLE `mview_subcompany2member` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_subcompany2member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mview_subcompany2senior`
--

DROP TABLE IF EXISTS `mview_subcompany2senior`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mview_subcompany2senior` (
  `date_unix` bigint NOT NULL,
  `subcompany_id` int NOT NULL,
  `senior_id` int NOT NULL,
  `game_id` int NOT NULL,
  `up_id` int NOT NULL,
  `turnover` decimal(20,4) DEFAULT NULL,
  `valid_amount` decimal(20,4) DEFAULT NULL,
  `winloss` decimal(20,4) DEFAULT NULL,
  `commission` decimal(20,4) DEFAULT NULL,
  `total` decimal(20,4) DEFAULT NULL,
  `agent` decimal(20,4) DEFAULT NULL,
  `agent_commission` decimal(20,4) DEFAULT NULL,
  `agent_total` decimal(20,4) DEFAULT NULL,
  `master` decimal(20,4) DEFAULT NULL,
  `master_commission` decimal(20,4) DEFAULT NULL,
  `master_total` decimal(20,4) DEFAULT NULL,
  `senior` decimal(20,4) DEFAULT NULL,
  `senior_commission` decimal(20,4) DEFAULT NULL,
  `senior_total` decimal(20,4) DEFAULT NULL,
  `subcompany` decimal(20,4) DEFAULT NULL,
  `subcompany_commission` decimal(20,4) DEFAULT NULL,
  `subcompany_total` decimal(20,4) DEFAULT NULL,
  `company` decimal(20,4) DEFAULT NULL,
  `company_commission` decimal(20,4) DEFAULT NULL,
  `company_total` decimal(20,4) DEFAULT NULL,
  `stake_count` int DEFAULT NULL,
  `players` int DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`date_unix`,`subcompany_id`,`senior_id`,`game_id`,`up_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mview_subcompany2senior`
--

LOCK TABLES `mview_subcompany2senior` WRITE;
/*!40000 ALTER TABLE `mview_subcompany2senior` DISABLE KEYS */;
/*!40000 ALTER TABLE `mview_subcompany2senior` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-17 20:39:21
